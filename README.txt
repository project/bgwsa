Synopsis
---------
This module purges the cached website URLs using BitGravity Web Accelerator Webservice.

Main Module: bitgravity.module
Dependent Modules: expire

Instructions
------------
1. Install the bitgravity module and its dependencies. 
   Normally sites/all/modules
2. Go to admin/config/development/bitgravity/api and provide required information.
3. Configure Cache Expiration  module (admin/config/system/expire).
3.1 Select External expiration radio button.
